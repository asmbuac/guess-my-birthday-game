from random import randint

month_low = 1
month_high = 12
year_low = 1924
year_high = 2004

name = input("Hi! What is your name? ")

for guess in range(5):
    month_guess = randint(month_low, month_high)
    year_guess = randint(year_low, year_high)

    print(f"Guess {guess + 1}: {name} were you born in {month_guess}/{year_guess}?")

    month_response = input("Is the month correct? Yes, later, or earlier? (all lowercase) ")
    year_response = input("Is the year correct? Yes, later, or earlier? (all lowercase) ")

    if month_response == "yes" and year_response == "yes":
        print("I knew it!")
        exit()
    elif guess == 4:
        print("I have other things to do. Good bye.")
    elif month_response == "yes" and year_response == "later":
        print("Drat! Lemme try again!")
        month_low = month_guess
        month_high = month_guess
        year_low = year_guess + 1
    elif month_response == "yes" and year_response == "earlier":
        print("Drat! Lemme try again!")
        month_low = month_guess
        month_high = month_guess
        year_high = year_guess - 1
    elif month_response == "later" and year_response == "yes":
        print("Drat! Lemme try again!")
        month_low = month_guess + 1
        year_low = year_guess
        year_high = year_guess
    elif month_response == "earlier" and year_response == "yes":
        print("Drat! Lemme try again!")
        month_high = month_guess - 1
        year_low = year_guess
        year_high = year_guess
    elif month_response == "later" and year_response == "later":
        print("Drat! Lemme try again!")
        month_low = month_guess + 1
        year_low = year_guess + 1
    elif month_response == "later" and year_response == "earlier":
        print("Drat! Lemme try again!")
        month_low = month_guess + 1
        year_low = year_guess - 1
    elif month_response == "earlier" and year_response == "earlier":
        print("Drat! Lemme try again!")
        month_high = month_guess - 1
        year_high = year_guess - 1
    elif month_response == "earlier" and year_response == "later":
        print("Drat! Lemme try again!")
        month_high = month_guess - 1
        year_high = year_guess + 1
