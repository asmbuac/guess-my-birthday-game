from random import randint

import random

year_low = 1924
year_high = 2004
months = ["January",
"February",
"March",
"April",
"May",
"June",
"July",
"August",
"September",
"October",
"November",
"December"]

name = input("Hi! What is your name? ")

for guess in range(5):
    month_guess = random.choice(months)
    year_guess = randint(year_low, year_high)

    print(f"Guess {guess + 1}: {name} were you born in {month_guess} {year_guess}?")

    month_response = input("Is the month correct? Yes or no? (all lowercase) ")
    year_response = input("Is the year correct? Yes, later, or earlier? (all lowercase) ")

    if month_response == "yes" and year_response == "yes":
        print("I knew it!")
        exit()
    elif guess == 4:
        print("I have other things to do. Good bye.")
    elif month_response == "yes" and year_response == "later":
        print("Drat! Lemme try again!")
        months = [month_guess]
        year_low = year_guess + 1
    elif month_response == "yes" and year_response == "earlier":
        print("Drat! Lemme try again!")
        months = [month_guess]
        year_high = year_guess - 1
    elif month_response == "no" and year_response == "yes":
        print("Drat! Lemme try again!")
        months.remove(month_guess)
        year_low = year_guess
        year_high = year_guess
    elif month_response == "no" and year_response == "later":
        print("Drat! Lemme try again!")
        months.remove(month_guess)
        year_low = year_guess + 1
    elif month_response == "no" and year_response == "earlier":
        print("Drat! Lemme try again!")
        months.remove(month_guess)
        year_high = year_guess - 1
