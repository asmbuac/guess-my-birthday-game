from random import randint

import random

day_low = 1
day_high = 31
year_low = 1924
year_high = 2004
months = ["January",
"February",
"March",
"April",
"May",
"June",
"July",
"August",
"September",
"October",
"November",
"December"]

name = input("Hi! What is your name? ")

for guess in range(5):
    month_guess = random.choice(months)
    day_guess = randint(day_low, day_high)
    year_guess = randint(year_low, year_high)

    print(f"Guess {guess + 1}: {name} were you born on {month_guess}/{day_guess}/{year_guess}?")

    month_response = input("Is the month correct? Yes, later, or earlier? (all lowercase) ")
    day_response = input("Is the day correct? Yes, later, or earlier? (all lowercase) ")
    year_response = input("Is the year correct? Yes, later, or earlier? (all lowercase) ")

    if month_response == "yes" and year_response == "yes":
        print("I knew it!")
        exit()
    elif guess == 4:
        print("I have other things to do. Good bye.")
    elif month_response == "yes":
        months = [month_guess]
    elif month_guess == months[0] and month_response == "later":
        del months[0]
    elif month_guess == months[1] and month_response == "earlier":
        del months[1:12]
    elif month_guess == months[1] and month_response == "later":
        del months[:2]
    elif month_guess == months[2] and month_response == "earlier":
        del months[2:12]
    elif month_guess == months[2] and month_response == "later":
        del months[:3]
    elif month_guess == months[3] and month_response == "earlier":
        del months[3:12]
    elif month_guess == months[3] and month_response == "later":
        del months[:4]
    elif month_guess == months[4] and month_response == "earlier":
        del months[4:12]
    elif month_guess == months[4] and month_response == "later":
        del months[:5]
    elif month_guess == months[5] and month_response == "earlier":
        del months[5:12]
    elif month_guess == months[5] and month_response == "later":
        del months[:6]
    elif month_guess == months[6] and month_response == "earlier":
        del months[6:12]
    elif month_guess == months[6] and month_response == "later":
        del months[:7]
    elif month_guess == months[7] and month_response == "earlier":
        del months[7:12]
    elif month_guess == months[7] and month_response == "later":
        del months[:8]
    elif month_guess == months[8] and month_response == "earlier":
        del months[8:12]
    elif month_guess == months[8] and month_response == "later":
        del months[:9]
    elif month_guess == months[9] and month_response == "earlier":
        del months[9:12]
    elif month_guess == months[9] and month_response == "later":
        del months[:10]
    elif month_guess == months[10] and month_response == "earlier":
        del months[10:12]
    elif month_guess == months[10] and month_response == "later":
        del months[:11]
    elif month_guess == months[11] and month_response == "earlier":
        del months[11]

    if day_response == "yes":
        day_low = day_guess
        day_high = day_guess
    elif day_response == "later":
        day_low = day_guess + 1
    elif day_response == "earlier":
        day_high = day_guess - 1

    if year_response == "yes":
        year_low = year_guess
        year_high = year_guess
    elif year_response == "later":
        year_low = year_guess + 1
    elif year_response == "earlier":
        year_high = year_guess - 1
